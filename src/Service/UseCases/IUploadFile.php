<?php

namespace App\Service\UseCases;

use App\Entity\Achat;
use App\Entity\Client;
use App\Entity\Evenement;
use App\Entity\Vehicule;
use App\Entity\Vente;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class IUploadFile implements IUploadFileInterface
{
    private ICreateInterface $iCreate;

    public function __construct(ICreateInterface $iCreate)
    {
        $this->iCreate = $iCreate;
    }

    public function query(UploadedFile $file): array
    {
        $result = [];

        $fileFolder = __DIR__."\\..\\..\\..\\public\\uploads\\";
        $fileName = md5(uniqid()) . $file->getClientOriginalName();

        $file->move($fileFolder, $fileName);

        $spreadsheet = IOFactory::load($fileFolder . $fileName);
        $spreadsheet->getActiveSheet()->removeRow(1);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        //Ne pas prendre en compte le denier élément (à cause des colonnes null)
        unset($sheetData[count($sheetData)]);

        foreach ($sheetData as $row) {
            $achatProperties = [
                "compteAffaire" => $row["A"],
                "dateAchat" => is_null($row["R"]) ? null : date_create(date("d-m-Y", strtotime($row["R"]))),
                "commentaireFacture" => $row["AD"],
                "intermediaireDeVente" => $row["AG"],
                "numeroDeFiche" => $row["D"],
                "libelleCivilite" => $row["E"],
                "nom" => $row["G"],
                "prenom" => $row["H"],
                "numeroEtNomDeLaVoie" => $row["I"],
                "complementAdresse1" => $row["J"],
                "codePostal" => $row["K"],
                "ville" => $row["L"],
                "telephoneDomicile" => $row["M"],
                "telephonePortable" => $row["N"],
                "telephoneJob" => $row["O"],
                "email" => $row["P"],
                "libelleMarque" => $row["T"],
                "libelleModele" => $row["U"],
                "version" => $row["V"],
                "vin" => $row["W"],
                "immatriculation" => $row["X"],
                "kilometrage" => $row["Z"],
                "libelleEnergie" => $row["AA"],
                "compteEvenement" => $row["B"],
                "compteDernierEvenement" => $row["C"],
                "origineEvenement" => $row["AI"],
                "dateEvenement" => is_null($row["AH"]) ? null : date_create(date("d-m-Y", strtotime($row["AH"]))),
                "dateDernierEvenement" => is_null($row["S"]) ? null : date_create(date("d-m-Y", strtotime($row["S"]))),
                "dateDeMiseEnCirculation" => is_null($row["Q"]) ? null : date_create(date("d-m-Y", strtotime($row["Q"]))),
                "vendeur" => is_null($row["AB"]) ? $row["AC"] : $row["AB"],
                "type" => $row["AE"],
                "numeroDeDossierVnVo" => $row["AF"],
            ];

            $result["Achat"][] = $this->iCreate->query(Achat::class, $achatProperties);
        }

        return $result;
    }
}