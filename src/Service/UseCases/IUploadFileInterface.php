<?php

namespace App\Service\UseCases;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface IUploadFileInterface
{
    public function query(UploadedFile $file): array;
}