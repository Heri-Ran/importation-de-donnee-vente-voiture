<?php

namespace App\Service\UseCases;

interface ICreateInterface
{
    public function query(string $class, ?array $properties = null): object;
}