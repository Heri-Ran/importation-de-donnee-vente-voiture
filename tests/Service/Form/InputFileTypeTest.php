<?php

namespace App\Tests\Service\Form;

use App\Service\Form\InputFileType;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\File\File;

class InputFileTypeTest extends TypeTestCase
{
    protected function iniSet(string $varName, string $newValue): void
    {
        $this->inputFileType = new InputFileType();
    }

    public function testGetForm(): void
    {
        $file = new File(__DIR__."/Fixtures/data.txt");
        $formData = [
            "fichier" => $file
        ];

        $form = $this->factory->create(InputFileType::class);
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        $this->assertIsArray($form->getData());

        #"Test passed!"
    }
}