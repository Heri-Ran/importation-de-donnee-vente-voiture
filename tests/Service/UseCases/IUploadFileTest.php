<?php

namespace App\Tests\Service\UseCases;

use App\Service\UseCases\ICreateInterface;
use App\Service\UseCases\IUploadFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class IUploadFileTest extends \PHPUnit\Framework\TestCase
{
    private IUploadFile $iUploadFile;

    private UploadedFile $file;

    private ICreateInterface $iCreate;

    protected function setUp(): void
    {
        $this->file = \Mockery::mock(UploadedFile::class);

        $this->iCreate = \Mockery::mock(ICreateInterface::class);

        $this->iUploadFile = new IUploadFile($this->iCreate);
    }

    // Malheureusement, Mockery ne peut pas mocker les methods statics.
//    public function testQuery(): void
//    {
//        $this->file->shouldReceive("getClientOriginalName")->andReturn("C:\\Users\\2880\\github\\import_client\\tests\\Service\\Form\\Fixtures\\data.txt");
//        $this->file->shouldReceive("move");
//        $this->assertIsArray($this->iUploadFile->query($this->file));
//    }
}